package com.kodevincere.fontmanager;

import android.app.Application;

import com.kodevincere.fontmanager.font.FontManager;

/**
 * Created by Jako43
 */
public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        FontManager.initialize(this);
    }
}
