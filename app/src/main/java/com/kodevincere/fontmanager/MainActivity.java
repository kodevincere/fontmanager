package com.kodevincere.fontmanager;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.kodevincere.fontmanager.font.Font;
import com.kodevincere.fontmanager.font.FontManager;

/**
 * Created by Jako43
 */
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextView tvGibson = (TextView) findViewById(R.id.tv_gibson);
        TextView tvRoboto = (TextView) findViewById(R.id.tv_roboto);

        FontManager.on()
                .setTypeface(Font.GIBSON_REGULAR, tvGibson)
                .setTypeface(Font.ROBOTO_BLACK, tvRoboto);



    }

}
