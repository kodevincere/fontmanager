package com.kodevincere.fontmanager.font;

/**
 * Created by Jako43
 */
public enum Font {

    GIBSON_REGULAR("gibson-regular.ttf"),
    ROBOTO_BLACK("Roboto-Black.ttf"),;

    private final String path;

    Font(String str) {
        this.path = str;
    }

    public String value() {
        return path;
    }
}
