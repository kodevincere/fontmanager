package com.kodevincere.fontmanager.font;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.widget.TextView;

import java.util.HashMap;

/**
 * Created by Jako43
 */
public class FontManager {

    private static FontManager instance;
    private static final HashMap<Font, Typeface> map = new HashMap<>();

    public static FontManager on() {
        if (instance == null) {
            instance = new FontManager();
        }
        return instance;
    }

    //Call this method in GlobalApplication
    public static void initialize(Context context) {
        for(Font f: Font.values()){
            if(!map.containsKey(f)) {
                map.put(f, Typeface.createFromAsset(context.getAssets(), f.value()));
            }
        }
    }

    //Method for put set TypeFace to view
    public FontManager setTypeface(Font font, View view) {
        if (view != null) {
            if (view instanceof TextView) {
                ((TextView) view).setTypeface(getTypeface(font));
            }// Include TextInputLayout and CollapsingToolbarLayout
        }

        return this;
    }

    public Typeface getTypeface(Font font) {
        return map.get(font);
    }
}
